<?php
/**
 * Template Name: Home Page Template
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

while ( have_posts() ) :
    the_post();
?>


<!-- Hero -->
<section id="section__hero" class="bg-primary py-5 p-lg-0">
    <div class="container">
        <div class="row align-items-center" style="min-height: 80vh;">
            <div class="col-12 col-lg-10 offset-lg-1 text-center">
                <h6 class="text-white text-uppercase overline">Recreational Services, LLC.</h6>
                <h1 class="text-white mt-4" style="font-size: 3.1825rem; line-height: 4.8125rem;">Summer doesn’t start until the toys are in the water</h1>
                <h3 class="text-white" style="font-size: 1.5rem; font-family: Lato; font-weight: normal;">Specializing in industry hardware and installation</h3>
                <a href="/contact" class="btn btn-primary mt-5">Get in Touch</a>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-12 text-left text-lg-center">
                <h1>Dock Sales for Northern Michigan</h1>
                <h3 class="text-muted">We go above and beyond the call of duty for our customers.</h3>
            </div>
        </div>
        <div class="row align-items-center mt-4">
            <div class="col-12 col-lg-6">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="/wp-content/uploads/2019/11/shorescreenrevolutionseriesdock.png" style="height: 350px; object-fit: cover;" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="/wp-content/uploads/2019/11/shorescreenrevolutionseriesdock.png" style="height: 350px; object-fit: cover;" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="/wp-content/uploads/2019/11/Docks.png" style="height: 350px; object-fit: cover;" class="d-block w-100" alt="...">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <ul>
                    <li>#1 Hoist & Dock Sales</li>
                    <li>#2 Hoist Transportation & Barge Deliveries</li>
                    <li>#3 Dock Installation & Repair</li>
                </ul>
                <a href="" class="btn btn-light mt-5">View Docks</a>
                <a href="" class="btn btn-light mt-5">View Shore Stations</a>
            </div>
        </div>
    </div>
</section>

<!-- Banner -->
<section style="background: url('/wp-content/uploads/2019/11/small-banner.svg'); background-size: auto; background-position: center; background-repeat: repeat-x;"></section>

<!-- Service Listings -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-4 text-center text-uppercase">
                <img src="/wp-content/uploads/2019/11/ship-duotone.svg" class="img-fluid mb-4" alt="Boat Duotone Icon">
                <a href="" class="btn btn-block btn-light">hoist & dock sales</a>
                <a href="" class="btn btn-block btn-light">locally made twin bay docks</a>
                <a href="" class="btn btn-block btn-light">dock design</a>
            </div>
            <div class="col-12 col-lg-4 text-center text-uppercase">
                <img src="/wp-content/uploads/2019/11/anchor-duotone.svg" class="img-fluid mb-4" alt="Anchor Duotone Icon">
                <a href="" class="btn btn-block btn-light">Hoist transport & barge deliveries</a>
                <a href="" class="btn btn-block btn-light">waterfront services</a>
                <a href="" class="btn btn-block btn-light">Vinyl dock sales</a>
            </div>
            <div class="col-12 col-lg-4 text-center text-uppercase">
                <img src="/wp-content/uploads/2019/11/water-duotone.svg" class="img-fluid mb-4" alt="Water Duotone Icon">
                <a href="" class="btn btn-block btn-light">dock installation & repair</a>
                <a href="" class="btn btn-block btn-light">dock removal</a>
                <a href="" class="btn btn-block btn-light">shore station hoists</a>
            </div>
        </div>
    </div>
</section>


<!-- CTA -->
<section style="background: url('/wp-content/uploads/2019/11/Pattern-Background.svg'); background-size: cover; background-position: center; background-repeat: no-repeat;">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h6 class="text-primary text-uppercase overline">Get a jumpstart on summer</h6>
                <h1 class="text-white mt-4">Give us a call today</h1>
                <a href="" class="btn btn-primary mt-4">(231) 633-1665</a>
            </div>
        </div>
    </div>
</section>


<!-- Shore Stations and Docks -->
<section class="bg-light">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-header">Shore Stations</h5>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                Docks
            </div>
        </div>
    </div>
</section>


<!-- About -->
<section class="p-0">
    <div class="container-fluid p-0">
        <div class="row align-items-center">
            <div class="col-12 col-lg-5">
                <img src="/wp-content/uploads/2019/11/shorescreenrevolutionseriesdock.png" class="img-fluid" alt="">
            </div>
            <div class="col-12 col-lg-5 offset-lg-1">
                <div class="row">
                    <div class="col-12">
                        <h1 class="h2">Northern Michigan’s #1 Provider of Waterfront Solutions</h1>
                        <h3 class="font-style-display">About Us</h3>
                    </div>
                </div>
                <span style="width: 20%; height: 2px;" class="bg-primary rounded d-block mt-4"></span>
                <p class="lead mt-4">We are a locally owned company that provides sales for shore station hoists, locally made Twin bay docks, dock design, waterfront consultation, and waterfront services.</p>
            </div>
        </div>
    </div>
</section>


<style>
    #section__hero {
        background: url('/wp-content/uploads/2019/11/home-hero.png');
        background-size: cover;
        background-position: center center;
        background-repeat: no-repeat;
    }
</style>

<?php
endwhile;

get_footer();
