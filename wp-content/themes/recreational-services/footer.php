<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>


<!-- Pre-Footer CTA -->
<section style="background: url('/wp-content/uploads/2019/11/Pattern-Background.svg'); background-size: cover; background-position: center; background-repeat: no-repeat;">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h6 class="text-primary text-uppercase overline">Summer won't wait</h6>
                <h1 class="text-white mt-4">Why should you?</h1>
                <a href="" class="btn btn-primary mt-4">(231) 633-1665</a>
            </div>
        </div>
    </div>
</section>


<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper bg-primary" id="wrapper-footer">

	<div class="<?php echo esc_attr( $container ); ?>">

		<!-- Footer Columns -->
		<div class="row">

		
			<!-- About -->
			<div class="col-12 col-lg-4 text-white mt-5 mt-lg-0">
				<h4 class="mb-4">Recreational Services, LLC.</h4>
				<p>We go above and beyond the call of duty for our customers. Our company specializes in dock sales, dock installation and removal, dock repair and water trampolines.</p>
			</div>

			<!-- Navigation -->
			<div class="col-12 col-lg-4 text-white mt-5 mt-lg-0">
				<h4 class="mb-4">Navigation</h4>
				<a href="/" class="btn btn-primary btn-block text-left">Home</a>
				<a href="/shore-stations" class="btn btn-primary btn-block text-left">Shore Stations</a>
				<a href="/docks" class="btn btn-primary btn-block text-left">Docks</a>
				<a href="/contact" class="btn btn-primary btn-block text-left">Contact</a>
			</div>

			<!-- Contact -->
			<div class="col-12 col-lg-4 text-white mt-5 mt-lg-0">
				<h4 class="mb-4">Contact</h4>
				<a href="tel:2316331655" class="btn btn-primary btn-block text-left"><i class="fa fa-phone mr-2"></i>1 (231) 633-1655</a>
				<a href="mtilto:john@recreationalserviceslls.com" class="btn btn-primary btn-block text-left"><i class="fa fa-envelope mr-2"></i>john@recreationalservicesllc.com</a>
			</div>

			<div class="col-md-12">

				<footer class="site-footer" id="colophon">

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->


<!-- Footer Bottom -->
<section class="bg-secondary py-4">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="row justify-content-between">
					<p class="m-0 text-white-50">&copy; <?php echo date('Y'); ?> Recreational Services, LLC.</p>
					<span class="text-white-50">Built with <i class="fa fa-heart fa-xs"></i> by <a href="https://legendarylion.com" class="text-white-50"><u>Legendary Lion</u></a></span>	
				</div>

				<div class="site-info">

				</div><!-- .site-info -->
			</div>
		</div>
	</div>
</section>


</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

